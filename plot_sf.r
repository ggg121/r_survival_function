#######################################################################
#
# plot_sf.r    15 Jan 2015
#
# Author: Gregory Garner (ggg121@psu.edu)
#
# Function that plots the survival function of a given vector of data
#
# To use this function, simply source this file:
#   source("plot_sf.r")
#
# Version History:
#   1.0 - 15 Jan 2015 - Initial coding (G.G.)
#   1.1 - 03 Feb 2015 - Added support for left.tail (G.G.)
#   1.2 - 24 Jun 2016 - Added sf.point, .FindX, and .FindY
#                       functions (G.G.)
#
# Note: I wrote this code because I've been asked fairly often for
# code that does this type of plot.  The original code I had included
# a lot of things the regular user wouldn't need, so I wrote a simple
# plot() wrapper to share with people.  It's not fancy, but it does the
# job.
#
# THIS CODE IS PROVIDED AS-IS WITH NO WARRANTY (NEITHER EXPLICIT
# NOT IMPLICIT).  I SHARE THIS CODE IN HOPES THAT IT IS USEFUL, 
# BUT I AM NOT LIABLE FOR THE BEHAVIOR OF THIS CODE IN YOUR OWN
# APPLICATION.  YOU ARE FREE TO SHARE THIS CODE SO LONG AS THE
# AUTHOR(S) AND VERSION HISTORY REMAIN INTACT.
#
#######################################################################

#######################################################################
# Function Name: plot.sf
# Parameters:
#   x - Data vector to be plotted
#   xlab - X-axis label (default = name of the data object passed
#          to the function)
#   left.tail - Should the plot highlight the left-tail instead of the 
#               right-tail? (default = F)
#   ylab - Y-axis label (default = SF  [1 - Cum. Freq.]) 
#   make.plot - Boolean value determining whether or not to make a plot
#               (default = T)
#   ... - Other parameters to be passed to the plot() function
#
# The function invisibly returns the survival function value for the
# passed data vector, even if make.plot is false.  This is useful when
# you want to customize your plot or use the survival function data
# in further analyses.
#
# See the accompanying "plot_sf_example.r" file for usage and examples.
#
#######################################################################

plot.sf <- function(x, xlab=deparse(substitute(x)), left.tail=F,
  ylab=ifelse(left.tail, "SF [Cum. Freq.]", "SF  [1 - Cum. Freq.]"),
  make.plot=T, ...)
{
  x.na <- na.omit(x)
  num.x <- length(x.na)
  num.ytics <- floor(log10(num.x))
  sf <- seq(1,1/num.x,by=-1/num.x)
  
  if(left.tail){
    order.x <- order(x.na, decreasing=T)
    order.sf <- sf[order(order.x)]
    
  }  else {
    order.x <- order(x.na)
    order.sf <- sf[order(order.x)]
  }
  
  if(make.plot) {
    plot(x.na[order.x], sf, log="y", xlab=xlab, ylab=ylab, yaxt="n", ...)
    axis(2, at=10^(-num.ytics:0), label=parse(text=paste("10^", -num.ytics:0, sep="")), las=1)
  }
  invisible(list(x=x.na[order.x], y=sf))
}

#######################################################################
# Function Name: sf.point
# Parameters:
#   data - Data vector over which the point is calculated
#   x - Data value for which a SF value is needed (default = NULL)
#   y - SF value for which a data value is needed (default = NULL)
#   log.point - Should the y point be interpreted/returned in
#               log space?  (default = FALSE)
#   ... - Parameters passed to other functions
#
# The function determines from the input parameters the other half of
# the point required (i.e. if you provide an x, it'll return a y). 
#
# The parameter log.point should be used only for plotting purposes.
# Since survival function plots are in log-space, care must be taken
# when appending points to an existing plot.  Setting log.point to
# TRUE will interpret y as a SF value in log-space or return y
# as a value in log-space. For numerical calculations using the
# survival function values, log.point should be FALSE (default).
#
# NOTE: It's MUCH more efficient to calculate a large number of SF
# values using the invisible return from the plot.sf function above.
# This function is useful if you want to point out specific values
# on a plot.
#
#######################################################################
sf.point <- function(data, x=NULL, y=NULL, log.point=FALSE, ...) {
  
  # Test which value we have and which we need to calculate
  if(is.null(x) & is.null(y)){
    stop("Must provide value for either x or y")
  }
  if(!is.null(x) & !is.null(y)){
    stop("Both x and y provided...not sure which to calculate")
  }
  
  if(is.null(x)) {
    val <- .FindX(data, y, log.point, ...)
  }
  else {
    val <- .FindY(data, x, log.point, ...)
  }
  
  return(val)
  
}


.FindY <- function(x, val, log.point, ...){
  
  # Is val outside the range?
  if(max(x) < val | min(x) > val){
    return(NA)
  }
  
  # Sort the vector and get its length
  sort.x <- sort(x)
  num.x <- length(x)
  sf <- seq(1,1/num.x, by=-1/num.x)
  
  # Determine the bounding values
  lb.i <- findInterval(val, sort.x)
  ub.i <- lb.i + 1
  
  # Note, if the lower bound is the last
  # element in the vector, return the 
  # SF value for the last item
  # (i.e. x[num.x] == val)
  if (lb.i == num.x) {
    return(1/num.x)
  }
  
  # Set the lower and upper bounds for interpolation
  lb.x <- sort.x[lb.i]
  ub.x <- sort.x[ub.i]
  lb.sf <- sf[lb.i]
  ub.sf <- sf[ub.i]
  
  # Fit the interpolation
  ifelse(log.point, 
         yes = 10^(((val - lb.x)/(ub.x - lb.x)) * (log10(ub.sf) - log10(lb.sf)) + log10(lb.sf)),
         no = ((val - lb.x)/(ub.x - lb.x)) * (ub.sf - lb.sf) + lb.sf
  )
}


.FindX <- function(x, val, log.point, ...){
  
  # Sort the vector and get its length
  sort.x <- sort(x, decreasing=T)
  num.x <- length(x)
  
  # Is val outside the range?
  if(val > 1 | val < 1/num.x){
    return(NA)
  }
  if(val == 1) {
    return(min(x, na.rm=T))
  }
  
  # Calculate the survival function values
  sf <- seq(1/num.x, 1, by=1/num.x)
  
  # Determine the bounding values
  lb.i <- findInterval(val, sf)
  ub.i <- lb.i + 1
  
  # Note, if the lower bound is the last
  # element in the SF values, return the 
  # data value for the last item
  # (i.e. sf[num.x] == val)
  if (lb.i == num.x) {
    return(sort.x[num.x])
  }
  
  # Set the lower and upper bounds for interpolation
  lb.x <- sort.x[lb.i]
  ub.x <- sort.x[ub.i]
  lb.sf <- sf[lb.i]
  ub.sf <- sf[ub.i]
  
  # Fit the interpolation
  ifelse(log.point, 
         yes = ((log10(val) - log10(lb.sf))/(log10(ub.sf) - log10(lb.sf))) * (ub.x - lb.x) + lb.x,
         no = ((val - lb.sf)/(ub.sf - lb.sf)) * (ub.x - lb.x) + lb.x
  )
  
}